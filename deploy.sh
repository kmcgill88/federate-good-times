
sam validate --template ./template.yaml
sam package \
    --template-file .aws-sam/build/template.yaml \
    --output-template-file .aws-sam/build/serverless-output.yaml \
    --s3-bucket devtech-artifacts
sam deploy \
    --template-file .aws-sam/build/serverless-output.yaml \
    --stack-name SAM-FEDERATION-EXAMPLE-APP \
    --capabilities CAPABILITY_NAMED_IAM

if [[ $1 == "delete" ]]; then
  aws cloudformation delete-stack --stack-name SAM-FEDERATION-EXAMPLE-APP
fi