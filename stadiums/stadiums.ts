import { ApolloServer, gql } from 'apollo-server-lambda';
import { GraphQLRequest, GraphQLResponse } from 'apollo-server-types';
import { buildFederatedSchema } from '@apollo/federation';

const typeDefs = gql`
  extend type Query {
    stadiums: [Stadium!]!
  }
  
  type Stadium {
    id: ID!
    name: String!
    location: String!
  }
`;

const resolvers = {
  Query: {
    stadiums: () => [
      {
        id: '1',
        name: 'US Bank Stadium',
        location: 'Minneapolis',
      },
      {
        id: '2',
        name: 'Arrow Head',
        location: 'Kansas City',
      },
      {
        id: '3',
        name: 'Soldier Field',
        location: 'Chicago',
      },
    ],
  },
};

let getContext = () => ({});
const server = new ApolloServer({
  schema: buildFederatedSchema([{ typeDefs, resolvers }]),
  context: () => getContext(),
});

exports.graphqlHandler = async (event: GraphQLRequest & { gatewayContext?: any }): Promise<GraphQLResponse> => {
  console.log('event', JSON.stringify(event));
  getContext = () => ({
    ...event.gatewayContext
  });
  return server.executeOperation(event);
}