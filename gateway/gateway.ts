import { ApolloServer } from 'apollo-server-lambda';
import {
  ApolloGateway,
  GraphQLDataSource,
  RemoteGraphQLDataSource,
  ServiceEndpointDefinition,
} from '@apollo/gateway';
import { APIGatewayProxyEvent, Context as LambdaContext } from 'aws-lambda';
import {
  GraphQLRequestContext,
  GraphQLResponse,
} from 'apollo-server-types';
import AWS = require("aws-sdk");
import { InvocationRequest } from 'aws-sdk/clients/lambda';

// https://www.apollographql.com/docs/federation/api/apollo-gateway/#class-remotegraphqldatasource
const gateway = new ApolloGateway({
  debug: true,
  serviceList: [
    // { name: 'teams', url: 'http://localhost:4001/' },
    // { name: 'stadiums', url: 'http://localhost:4002/' },
    { name: 'teams', url: 'arn:aws:lambda:us-east-2:244561594477:function:SAM-FEDERATION-EXAMPLE-APP-TeamsFunction-15954229CBHLN' },
    { name: 'stadiums', url: 'arn:aws:lambda:us-east-2:244561594477:function:SAM-FEDERATION-EXAMPLE-APP-StadiumsFunction-1N9LYFETIRM6T' },
  ],
  // https://www.apollographql.com/docs/federation/api/apollo-gateway/#buildservice
  buildService: (serviceDefinition: ServiceEndpointDefinition) => {
    if (serviceDefinition.url.includes('arn:aws:lambda:')) {
      // This is a lambda, use custom data source
      return new AWSLambdaGraphQLDataSource(
        serviceDefinition.url,
        serviceDefinition.name,
      );
    }

    // append all headers
    return new AppendHeadersDataSource(serviceDefinition);
  },
});


class AWSLambdaGraphQLDataSource implements GraphQLDataSource {
  serviceName!: string;
  url!: string;

  constructor(url: string, serviceName: string) {
    this.url = url;
    this.serviceName = serviceName;
  }

  async process(graphQLRequestContext: Pick<GraphQLRequestContext<Record<string, any>>, 'request' | 'context'>): Promise<GraphQLResponse> {
    console.log('process', JSON.stringify(graphQLRequestContext));

    const { http, ...requestWithout } = graphQLRequestContext.request;

    if (!this.url.includes('arn:aws:lambda:')) {
      throw new Error(`Invalid url for AWSLambdaGraphQLDataSource: ${this.url}`);
    }
    const response = await invoke(this.url, {
      ...requestWithout,
      gatewayContext: graphQLRequestContext?.context,
    });
    return JSON.parse(response.Payload.toString());
  }
}

class AppendHeadersDataSource extends RemoteGraphQLDataSource {
  willSendRequest({ request, context }) {
    Object.keys(context).forEach((headerKey) => {
      request.http.headers.set(headerKey, context[headerKey]);
    });
  }
}

const invoke = (lambdaName: string, event: object): Promise<AWS.Lambda.InvocationResponse> =>
  new Promise((resolve, reject) => {
    const payloadString = JSON.stringify(event);
    console.log(`Calling "${lambdaName}" with payload:`, payloadString);
    const params: InvocationRequest = {
      FunctionName: lambdaName,
      InvocationType: "RequestResponse",
      Payload: payloadString,
    };

    new AWS.Lambda().invoke(params, (err: AWS.AWSError, data: AWS.Lambda.InvocationResponse) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });


const server = new ApolloServer({
  gateway,
  subscriptions: false,
  playground: true,
  context: ({ event }) => {
    // Note! This example uses the `{ event, context }` object to access headers,
    // but the arguments received by `context` vary by integration.
    // This means they will vary for Express, Koa, Lambda, etc.!
    //
    // To find out the correct arguments for a specific integration,
    // see the `context` option in the API reference for `apollo-server`:
    // https://www.apollographql.com/docs/apollo-server/api/apollo-server/#middleware-specific-context-fields
    // const lambdaEvent = event as APIGatewayProxyEvent;
    const myContext = {
      foo: 'bar',
      ...event
    };
    console.log('gateway context', JSON.stringify(myContext))

    return myContext;
  },
});

exports.graphqlHandler = server.createHandler();
