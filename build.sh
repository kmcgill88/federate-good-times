#! /bin/bash
set -e
function build() {
    cd $1
    npm run build
    cd ..
}

build gateway
build stadiums
build teams
sam build