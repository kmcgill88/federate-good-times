import { ApolloServer, gql } from 'apollo-server-lambda';
import { GraphQLRequest, GraphQLResponse } from 'apollo-server-types';
import { buildFederatedSchema } from '@apollo/federation';

const typeDefs = gql`
  extend type Query {
    teams: [Team!]!
  }

  type Team {
    id: ID!
    name: String!
    league: String!
  }
`;

const resolvers = {
  Query: {
    teams: () => [
      {
        id: '1',
        name: 'Vikings',
        league: 'NFL',
      },
      {
        id: '2',
        name: 'Chiefs',
        league: 'NFL',
      },
      {
        id: '3',
        name: 'Bears',
        league: 'NFL',
      },
    ],
  },
};

let getContext = () => ({});
const server = new ApolloServer({
  schema: buildFederatedSchema([{ typeDefs, resolvers }]),
  context: () => getContext(),
});

exports.graphqlHandler = async (event: GraphQLRequest & { gatewayContext?: any }): Promise<GraphQLResponse> => {
  console.log('event', JSON.stringify(event));
  getContext = () => ({
    ...event.gatewayContext
  });
  return server.executeOperation(event);
}